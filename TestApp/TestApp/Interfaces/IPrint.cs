﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Models;

namespace TestApp.Interfaces
{
    public interface IPrinter
    {
        void Print(Figure figure );
    }
}
