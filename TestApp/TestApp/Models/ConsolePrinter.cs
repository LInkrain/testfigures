﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Interfaces;

namespace TestApp.Models
{
    class ConsoleFigurePrinter : IPrinter
    {
        public void Print(Figure figure)
        {
            Console.WriteLine($"Привет! я {figure.Name}. Моя площадь: {figure.Square}, периметр: {figure.Perimeter}");
        }
    }
    class ConsoleCirclePrinter : IPrinter
    {
        public void Print(Figure figure)
        {
            Console.WriteLine($"Привет! я {figure.Name}. Моя площадь: {figure.Square}, периметр: {figure.Perimeter}, радиус: {(figure as Circle).Radius}");
        }
    }
    class ConsoleTrianglePrinter : IPrinter
    {
        public void Print(Figure figure)
        {
            string IsRectangle(bool isRectangle)
            {
                if(isRectangle)
                {
                    return "прямоугольный";
                }
                else
                {
                    return "обычный";
                }
            }

            Console.WriteLine($"Привет! я {figure.Name}. Моя площадь: {figure.Square}, периметр: {figure.Perimeter}, мои стороны : a =  {(figure as Triangle).A}, " +
                $"b =  {(figure as Triangle).B}, c =  {(figure as Triangle).C}, я {IsRectangle((figure as Triangle).IsRectangle)}");
        }
    }
}
