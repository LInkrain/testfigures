﻿using System;
using TestApp.Interfaces;

namespace TestApp.Models
{
    public abstract class Figure
    {
        abstract public double Square { get; }
        abstract public double Perimeter { get; }
        abstract public string Name { get; }
        protected IPrinter printer;
        public Figure(IPrinter printer)
        {
            this.printer = printer;
        }
        public abstract void GetInfo();
    }

    public class Circle : Figure
    {
        double radius;

        public  Circle(double radius, IPrinter printer): base(printer)
        {
            this.radius = radius;
        }

        public double Radius
        {
            get
            {
                return radius;
            }
            set
            {
                radius = value;
            }
        }

        public override double Square => radius * radius * Math.PI;
        public override double Perimeter => 2 * Math.PI * radius;
        public override string Name => "Круг";

        public override void GetInfo()
        {
            printer.Print(this);
        }
    }

    public class Triangle : Figure
    {
        double a;
        double b;
        double c;

        public Triangle(double a, double b, double c, IPrinter printer) : base(printer)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public double A
        {
            get
            {
                return a;
            }
            set
            {
                a = value;
            }
        }

        public double B
        {
            get
            {
                return b;
            }
            set
            {
                b = value;
            }
        }

        public double C
        {
            get
            {
                return c;
            }
            set
            {
                c = value;
            }
        }

        public override double Square
        {
            get
            {
                if (IsRectangle)
                {
                    return (a * b) / 2;
                }
                else
                {
                    double p = Perimeter;
                    return Math.Sqrt(p * (p - 2) * (p - 2) * (p - c));
                }
            }
        }

        public bool IsRectangle => (a * a + b * b) == (c * c);
        public override double Perimeter => (a + b + c) / 2;
        public override string Name => "Треугольник";

        public override void GetInfo()
        {
            printer.Print(this);
        }
    }

}
