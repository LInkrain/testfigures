﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.Models;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Figure figure = new Circle(12, new ConsoleFigurePrinter());
            figure.GetInfo();

            figure = new Triangle(2,3,4, new ConsoleFigurePrinter());
            figure.GetInfo();

            figure = new Circle(12, new ConsoleCirclePrinter());
            figure.GetInfo();

            figure = new Triangle(2, 3, 4, new ConsoleTrianglePrinter());
            figure.GetInfo();

            figure = new Triangle(3, 4, 5, new ConsoleTrianglePrinter());
            figure.GetInfo();
        }
    }
}
